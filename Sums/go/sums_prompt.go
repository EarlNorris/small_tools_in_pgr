package main

import (
	"fmt"
	"os"
)

func greetUser() {
	fmt.Println("Hello, Earl!")
	fmt.Println("Welcome to the improved Go prompt script.")
	fmt.Println("--------------------------------------------")
}

func addNumbers() {
	var num1, num2 float64

	fmt.Print("Enter the first number: ")
	fmt.Scan(&num1)

	fmt.Print("Enter the second number: ")
	fmt.Scan(&num2)

	result := num1 + num2
	fmt.Printf("Result: %f\n\n", result)
}

func subtractNumbers() {
	var num1, num2 float64

	fmt.Print("Enter the first number: ")
	fmt.Scan(&num1)

	fmt.Print("Enter the second number: ")
	fmt.Scan(&num2)

	result := num1 - num2
	fmt.Printf("Result: %f\n\n", result)
}

func multiplyNumbers() {
	var num1, num2 float64

	fmt.Print("Enter the first number: ")
	fmt.Scan(&num1)

	fmt.Print("Enter the second number: ")
	fmt.Scan(&num2)

	result := num1 * num2
	fmt.Printf("Result: %f\n\n", result)
}

func divideNumbers() {
	var num1, num2 float64

	fmt.Print("Enter the numerator: ")
	fmt.Scan(&num1)

	fmt.Print("Enter the denominator: ")
	fmt.Scan(&num2)

	if num2 == 0 {
		fmt.Println("Cannot divide by zero!\n")
		return
	}

	result := num1 / num2
	fmt.Printf("Result: %f\n\n", result)
}

func percentage() {
	var num, percent float64

	fmt.Print("Enter the number: ")
	fmt.Scan(&num)

	fmt.Print("Enter the percentage to calculate: ")
	fmt.Scan(&percent)

	result := (num * percent) / 100
	fmt.Printf("%f%% of %f is: %f\n\n", percent, num, result)
}

func sumsMenu() {
	for {
		fmt.Println("Choose an arithmetic operation:")
		fmt.Println("1. Add two numbers")
		fmt.Println("2. Subtract two numbers")
		fmt.Println("3. Multiply two numbers")
		fmt.Println("4. Divide two numbers")
		fmt.Println("5. Percentage")
		fmt.Println("6. Return to main menu")

		var choice int
		fmt.Print("Enter your choice (1/2/3/4/5/6): ")
		fmt.Scan(&choice)

		switch choice {
		case 1:
			addNumbers()
		case 2:
			subtractNumbers()
		case 3:
			multiplyNumbers()
		case 4:
			divideNumbers()
		case 5:
			percentage()
		case 6:
			return
		default:
			fmt.Println("Invalid choice! Please choose a valid option.\n")
		}
	}
}

func main() {
	greetUser()

	for {
		fmt.Println("Choose an operation:")
		fmt.Println("1. Sums")
		fmt.Println("2. Exit")

		var choice int
		fmt.Print("Enter your choice (1/2): ")
		fmt.Scan(&choice)

		switch choice {
		case 1:
			sumsMenu()
		case 2:
			fmt.Println("Goodbye, Earl!")
			os.Exit(0)
		default:
			fmt.Println("Invalid choice! Please choose a valid option.\n")
		}
	}
}
