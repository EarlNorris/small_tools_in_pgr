import sys

def greet_user():
    """Greeting the user"""
    print("Hello, Earl!")
    print("Welcome to the improved Python prompt script.")
    print("--------------------------------------------\n")

def add_numbers():
    """Adding two numbers"""
    try:
        num1 = float(input("Enter the first number: "))
        num2 = float(input("Enter the second number: "))
        result = num1 + num2
        print(f"Result: {result}\n")
    except ValueError:
        print("Please enter valid numbers!\n")

def subtract_numbers():
    """Subtracting two numbers"""
    try:
        num1 = float(input("Enter the first number: "))
        num2 = float(input("Enter the second number: "))
        result = num1 - num2
        print(f"Result: {result}\n")
    except ValueError:
        print("Please enter valid numbers!\n")

def multiply_numbers():
    """Multiplying two numbers"""
    try:
        num1 = float(input("Enter the first number: "))
        num2 = float(input("Enter the second number: "))
        result = num1 * num2
        print(f"Result: {result}\n")
    except ValueError:
        print("Please enter valid numbers!\n")

def divide_numbers():
    """Dividing two numbers"""
    try:
        num1 = float(input("Enter the numerator: "))
        num2 = float(input("Enter the denominator: "))
        if num2 == 0:
            print("Cannot divide by zero!\n")
            return
        result = num1 / num2
        print(f"Result: {result}\n")
    except ValueError:
        print("Please enter valid numbers!\n")

def percentage():
    """Calculating percentage"""
    try:
        num = float(input("Enter the number: "))
        percent = float(input("Enter the percentage to calculate: "))
        result = (num * percent) / 100
        print(f"{percent}% of {num} is: {result}\n")
    except ValueError:
        print("Please enter valid numbers!\n")

def sums_menu():
    """Sums submenu"""
    while True:
        print("Choose an arithmetic operation:")
        print("1. Add two numbers")
        print("2. Subtract two numbers")
        print("3. Multiply two numbers")
        print("4. Divide two numbers")
        print("5. Percentage")
        print("6. Return to main menu")

        choice = input("Enter your choice (1/2/3/4/5/6): ")

        if choice == '1':
            add_numbers()
        elif choice == '2':
            subtract_numbers()
        elif choice == '3':
            multiply_numbers()
        elif choice == '4':
            divide_numbers()
        elif choice == '5':
            percentage()
        elif choice == '6':
            break
        else:
            print("Invalid choice! Please choose a valid option.\n")

def main():
    """Main function to provide the menu and prompt"""
    greet_user()

    while True:
        # Main Menu
        print("Choose an operation:")
        print("1. Sums")
        print("2. Exit")

        choice = input("Enter your choice (1/2): ")

        if choice == '1':
            sums_menu()
        elif choice == '2':
            print("Goodbye, Earl!")
            sys.exit(0)
        else:
            print("Invalid choice! Please choose a valid option.\n")

if __name__ == "__main__":
    main()
